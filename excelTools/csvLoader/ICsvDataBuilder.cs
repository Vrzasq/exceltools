﻿namespace excelTools.csvLoader
{
    public interface ICsvDataBuilder<T>
    {
        T Build(string[] data);
    }
}