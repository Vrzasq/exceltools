﻿using System.Threading.Tasks;

namespace excelTools.csvLoader
{
    public interface ICsvDataBuilderAsync<T>
    {
        Task BuildAsync(string[] data);
        Task<T> GetTAsync();
    }
}