﻿using System.Collections.Generic;
using OfficeOpenXml;
using System.Threading.Tasks;

namespace excelTools.excelCreator
{
    public enum BuildType
    {
        STANDARD,
        MULTIPLE,
        SHT_PROVIDED
    }

    public class ExcelBuilder
    {
        public static ExcelPackage GetExcel(string shtName, string[] headers)
        {
            ExcelPackage file = new ExcelPackage();
            ExcelWorksheet sh = file.Workbook.Worksheets.Add(shtName);
            for (int i = 0; i < headers.Length; i++)
            {
                sh.Cells[1, i + 1].Value = headers[i];
            }
            return file;
        }

        public static ExcelPackage GetExcel(IList<string> shtNames, IList<string[]> multipleHeaders)
        {
            ExcelPackage file = new ExcelPackage();

            for (int i = 0; i < shtNames.Count; i++)
            {
                ExcelWorksheet sh = file.Workbook.Worksheets.Add(shtNames[i]);
                for (int j = 0; j < multipleHeaders[i].Length; j++)
                {
                    sh.Cells[1, j + 1].Value = multipleHeaders[j];
                }
            }

            return file;
        }
    }


    public class ExcelBuilder<T> : ExcelBuilder
    {
        private IEnumerable<T> thingToDisplay;
        private IColumnSelector<T> selector;
        private ExcelPackage excelFile = new ExcelPackage();
        private ExcelWorksheet sht;
        private string[] headers;

        private IList<IEnumerable<T>> thingsToDisplay;
        private IList<string[]> multipleHeaders;
        private IList<string> shtNames;
        private IList<IColumnSelector<T>> selectors;

        private bool builded = false;
        private BuildType buildType;


        public ExcelBuilder(ExcelPackage excelFile, int shtToRead, IEnumerable<T> thingToDisplay, IColumnSelector<T> selector)
        {
            this.excelFile = excelFile;
            sht = excelFile.Workbook.Worksheets[shtToRead];
            this.thingToDisplay = thingToDisplay;
            this.selector = selector;
            buildType = BuildType.SHT_PROVIDED;
        }

        public ExcelBuilder(string shtName, string[] headers, IEnumerable<T> thingToDisplay, IColumnSelector<T> selector)
        {
            sht = excelFile.Workbook.Worksheets.Add(shtName);
            this.thingToDisplay = thingToDisplay;
            this.headers = headers;
            this.selector = selector;
            buildType = BuildType.STANDARD;
        }

        public ExcelBuilder(IList<string> shtNames, IList<string[]> multipleHeaders, IList<IEnumerable<T>> thingsToDisplay, IList<IColumnSelector<T>> selectors)
        {
            this.shtNames = shtNames;
            this.multipleHeaders = multipleHeaders;
            this.thingsToDisplay = thingsToDisplay;
            this.selectors = selectors;
            buildType = BuildType.MULTIPLE;
        }

        public void Build(int startRow = 2)
        {
            if (!builded)
            {
                switch (buildType)
                {
                    case BuildType.STANDARD:
                        CreateHeaders();
                        PasteDataToExcel(startRow);
                        break;
                    case BuildType.MULTIPLE:
                        CreateMultipleSheetsHeaders();
                        PasteMultipleDataToExcel();
                        break;
                    case BuildType.SHT_PROVIDED:
                        PasteDataToExcel(startRow);
                        break;
                    default:
                        break;
                }
            }

            builded = true;
        }

        public ExcelPackage GetExcelFile()
        {
            return excelFile;
        }

        private void PasteDataToExcel(int startRow)
        {
            int i = startRow;
            foreach (var item in thingToDisplay)
            {
                selector.PasteToExcel(item, i, sht);
                i++;
            }
        }

        private void CreateHeaders()
        {
            for (int i = 0; i < headers.Length; i++)
                sht.Cells[1, i + 1].Value = headers[i];
        }

        private void CreateMultipleSheetsHeaders()
        {
            for (int i = 0; i < shtNames.Count; i++)
            {
                ExcelWorksheet sh = excelFile.Workbook.Worksheets.Add(shtNames[i]);
                for (int j = 0; j < multipleHeaders[i].Length; j++)
                {
                    sh.Cells[1, j + 1].Value = multipleHeaders[i][j];
                }
            }
        }

        private void PasteMultipleDataToExcel()
        {
            int length = excelFile.Workbook.Worksheets.Count;
            for (int i = 1; i <= length; i++)
            {
                ExcelWorksheet sh = excelFile.Workbook.Worksheets[i];
                int row = 2;
                foreach (var item in thingsToDisplay[i - 1])
                {
                    selectors[i - 1].PasteToExcel(item, row, sh);
                    row++;
                }
            }
        }
    }
}