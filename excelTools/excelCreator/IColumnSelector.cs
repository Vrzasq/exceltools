﻿using OfficeOpenXml;

namespace excelTools.excelCreator
{
    public interface IColumnSelector<T>
    {
        void PasteToExcel(T item, int row, ExcelWorksheet sht);
    }
}