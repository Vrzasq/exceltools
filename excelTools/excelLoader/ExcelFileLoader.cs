﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using OfficeOpenXml;
using System;
using System.Threading.Tasks;

namespace excelTools.excelLoader
{
    public class ExcelFileLoader
    {
        public event EventHandler OpenFileError;

        private int startingRow = 2;

        public List<T> GetData<T, U>(string filePath) where U : IDataBuilder<T>, new()
        {
            List<T> buildedData = new List<T>();
            object[,] rawData;

            FileInfo file = new FileInfo(filePath);

            if (file.Extension != ".xlsx")
            {
                FileInfo convertedfile = ConvertToXLSX(file);
                rawData = LoadExcelData(convertedfile);
                convertedfile.Delete();
            }
            else
                rawData = LoadExcelData(file);

            for (int i = 0; i < rawData.GetLength(0); i++)
            {
                if (!DataAvailable(rawData, i))
                    break;

                T t = new U().Build(rawData, i);
                buildedData.Add(t);
            }

            return buildedData;
        }

        public List<T> GetData<T, U>(object[,] data) where U : IDataBuilder<T>, new()
        {
            List<T> buildedData = new List<T>();
            object[,] rawData = data;

            for (int i = 0; i < rawData.GetLength(0); i++)
            {
                if (!DataAvailable(rawData, i))
                    break;

                T t = new U().Build(rawData, i);
                buildedData.Add(t);
            }

            return buildedData;
        }

        public async Task<List<T>> GetDataAsync<T, U>(string filePath, IProgress<string> progress) where U : IDataBuilderAsync<T>, new()
        {
            List<T> buildedData = new List<T>();
            object[,] rawData;

            FileInfo file = new FileInfo(filePath);

            if (file.Extension != ".xlsx")
            {
                FileInfo convertedfile = await ConvertToXLSXAsync(file).ConfigureAwait(false);
                rawData = await LoadExcelDataAsync(convertedfile);
                convertedfile.Delete();
            }
            else
                rawData = await LoadExcelDataAsync(file);

            for (int i = 0; i < rawData.GetLength(0); i++)
            {
                if (!await DataAvailableAsync(rawData, i))
                    break;

                T t = await GetTAsync(rawData, i, new U()).ConfigureAwait(false);
                buildedData.Add(t);
                progress.Report("Building from Excel: " + (i + 1).ToString() + " / " + rawData.GetLength(0));
            }

            return buildedData;
        }

        public async Task<List<T>> GetDataAsync<T, U>(string filePath, string sheetName, IProgress<string> progress) where U : IDataBuilderAsync<T>, new()
        {
            List<T> buildedData = new List<T>();
            object[,] rawData;

            FileInfo file = new FileInfo(filePath);

            if (file.Extension != ".xlsx")
            {
                FileInfo convertedfile = await ConvertToXLSXAsync(file).ConfigureAwait(false);
                rawData = await LoadExcelDataAsync(convertedfile, sheetName);
                convertedfile.Delete();
            }
            else
                rawData = await LoadExcelDataAsync(file, sheetName);

            for (int i = 0; i < rawData.GetLength(0); i++)
            {
                if (!await DataAvailableAsync(rawData, i))
                    break;

                T t = await GetTAsync(rawData, i, new U()).ConfigureAwait(false);
                buildedData.Add(t);
                progress.Report("Building from Excel: " + (i + 1).ToString() + " / " + rawData.GetLength(0));
            }

            return buildedData;
        }


        public async Task<List<T>> GetDataAsync<T, U>(object[,] data) where U : IDataBuilderAsync<T>, new()
        {
            List<T> buildedData = new List<T>();
            object[,] rawData = data;

            for (int i = 0; i < rawData.GetLength(0); i++)
            {
                if (!await DataAvailableAsync(rawData, i))
                    break;

                T t = await GetTAsync(rawData, i, new U()).ConfigureAwait(false);
                buildedData.Add(t);
            }

            return buildedData;
        }

        public void SetStartingRow(int row)
        { startingRow = row; }

        public Task SetStartingRowAsync(int row)
        {
            startingRow = row;
            return Task.FromResult(0);
        }

        private FileInfo ConvertToXLSX(FileInfo file)
        {
            Excel.Application excelApp = new Excel.Application();
            excelApp.DisplayAlerts = false;
            Excel.Workbooks temp = excelApp.Workbooks;
            Excel.Workbook wrk;
            try
            {
                wrk = temp.Open(file.FullName);
            }
            catch (IOException e)
            {
                OnOpenFileError();
                throw e;
            }

            wrk.SaveAs(Filename: file.Name + "x", FileFormat: Excel.XlFileFormat.xlOpenXMLWorkbook);
            FileInfo newfile = new FileInfo(wrk.FullName);
            excelApp.DisplayAlerts = true;

            wrk.Close();
            excelApp.Quit();
            Marshal.ReleaseComObject(wrk);
            Marshal.ReleaseComObject(temp);
            Marshal.ReleaseComObject(excelApp);

            return newfile;
        }


        private Task<FileInfo> ConvertToXLSXAsync(FileInfo file)
        {
            return Task.Run(() =>
            {
                Excel.Application excelApp = new Excel.Application();
                excelApp.DisplayAlerts = false;
                Excel.Workbooks temp = excelApp.Workbooks;
                Excel.Workbook wrk;
                try
                {
                    wrk = temp.Open(file.FullName);
                }
                catch (IOException e)
                {
                    OnOpenFileError();
                    throw e;
                }

                wrk.SaveAs(Filename: file.Name + "x", FileFormat: Excel.XlFileFormat.xlOpenXMLWorkbook);
                FileInfo newfile = new FileInfo(wrk.FullName);
                excelApp.DisplayAlerts = true;

                wrk.Close();
                excelApp.Quit();
                Marshal.ReleaseComObject(wrk);
                Marshal.ReleaseComObject(temp);
                Marshal.ReleaseComObject(excelApp);

                return newfile;
            });
        }

        private object[,] LoadExcelData(FileInfo file, string sheetName)
        {
            try
            {
                using (ExcelPackage ep = new ExcelPackage(file))
                {
                    using (ExcelWorksheet sht = ep.Workbook.Worksheets[sheetName])
                    {
                        return (object[,])sht.Cells[startingRow, 1, sht.Dimension.Rows, sht.Dimension.Columns].Value;
                    }
                }
            }
            catch (IOException e)
            {
                OnOpenFileError();
                throw e;
            }
        }

        private object[,] LoadExcelData(FileInfo file)
        {
            try
            {
                using (ExcelPackage ep = new ExcelPackage(file))
                {
                    using (ExcelWorksheet sht = ep.Workbook.Worksheets[1])
                    {
                        return (object[,])sht.Cells[startingRow, 1, sht.Dimension.Rows, sht.Dimension.Columns].Value;
                    }
                }
            }
            catch (IOException e)
            {
                OnOpenFileError();
                throw e;
            }
        }

        private Task<object[,]> LoadExcelDataAsync(FileInfo file, string sheetName)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (ExcelPackage ep = new ExcelPackage(file))
                    {
                        using (ExcelWorksheet sht = ep.Workbook.Worksheets[sheetName])
                        {
                            return (object[,])sht.Cells[startingRow, 1, sht.Dimension.Rows, sht.Dimension.Columns].Value;
                        }
                    }
                }
                catch (IOException e)
                {
                    OnOpenFileError();
                    throw e;
                }
            });
        }

        private Task<object[,]> LoadExcelDataAsync(FileInfo file)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (ExcelPackage ep = new ExcelPackage(file))
                    {
                        using (ExcelWorksheet sht = ep.Workbook.Worksheets[1])
                        {
                            return (object[,])sht.Cells[startingRow, 1, sht.Dimension.Rows, sht.Dimension.Columns].Value;
                        }
                    }
                }
                catch (IOException e)
                {
                    OnOpenFileError();
                    throw e;
                }
            });
        }

        protected virtual void OnOpenFileError()
        {
            if (OpenFileError != null)
                OpenFileError(this, EventArgs.Empty);
        }

        private async Task<T> GetTAsync<T>(object[,] data, int i, IDataBuilderAsync<T> builder)
        {
            await Task.Run(() =>
            {
                builder.BuildAsync(data, i).ConfigureAwait(false);
            });

            return await builder.GetTAsync();
        }

        private Task<bool> DataAvailableAsync(object[,] data, int i)
        {
            if (data[i, 0] == null)
                return Task.FromResult(false);
            if (data[i, 0].ToString() == string.Empty)
                return Task.FromResult(false);
            if (data[i, 0].ToString().ToUpper() == "##EOF##")
                return Task.FromResult(false);

            return Task.FromResult(true);
        }

        private bool DataAvailable(object[,] data, int i)
        {
            if (data[i, 0] == null)
                return false;
            if (data[i, 0].ToString() == string.Empty)
                return false;
            if (data[i, 0].ToString().ToUpper() == "##EOF##")
                return false;

            return true;
        }
    }
}