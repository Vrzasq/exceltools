﻿namespace excelTools.excelLoader
{
    public interface IDataBuilder<T>
    {
        T Build(object[,] data, int row);
    }
}