﻿using System.Threading.Tasks;

namespace excelTools.excelLoader
{
    public interface IDataBuilderAsync<T>
    {
        Task BuildAsync(object[,] data, int row);
        Task<T> GetTAsync();
    }
}